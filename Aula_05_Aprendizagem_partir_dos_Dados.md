## AULA::5 - Aprendizagem a partir dos Dados
## Data: 28-03-2017

#### 👉 Sumário
- Aprendizagem computacional:
- Tipos de aprendizagem
- Tarefas de aprendizagem
- Técnicas de aprendizagem
- Estimação de modelos
- Avaliação de modelos

#### 👉 Aprendizagem

- A maioria das abordagens de data mining é inspirada nas capacidades de aprendizagem dos sistemas biológicos e nos humanos
- Os humanos têm capacidades superiores para o reconhecimento de padrões
- Identificação de rostos, voz, cheiros, etc.
- Não se nasce com essas capacidades, são aprendidas e aperfeiçoadas através da interação com o ambiente

#### 👉 Introdução

- Conceito de aprendizagem
    - A aprendizagem é uma necessidade básica da vida
    - Existem vários tipos de aprendizagem
    - “Um indivíduo aprende a efetuar determinada tarefa transitando de um estado em que a tarefa não pode ser levada a cabo, para um estado em que a tarefa já possa ser efetuada, mantendo as circunstâncias.”

- Aprendizagem computacional
    - Quando um computador recebe um pedido que não consegue responder num momento, e, num momento posterior, mantendo as circunstâncias, consegue dar resposta
    - Definição insuficiente
    - Necessidade de generalização
    - Capacidade de aprender novas tarefas?

- Conceito de autoaprendizagem
    - Um computador com capacidade de gerar programas sozinho, de forma a responder a novas tarefas
    - Comparação entre humanos e computadores apresenta discrepância
    - Computadores são muito rápidos, mas sem criatividade ou auto-organização
    - Humanos muito mais lentos, mas com capacidade de resolver problemas, projetando soluções

- Metodologia de aprendizagem:
    - 1 - Observação
    - 2 - Análise
    - 3 - Teoria
    - 4 - Predição (Tentar projetar o que vai acontecer no futuro)

- Teoria da aprendizagem
    - Tendo uma hipótese formulada é impossível verificar a sua correção para todos os casos (a falsidade pode ser verificada com apenas uma observação)
    - Solução?
        - Testar no maior número de casos
        - Testar em casos novos
    - Os padrões encontrados não devem ser considerados teorias definitivas, mas apenas hipóteses temporárias

#### 👉 Aprendizagem Computacional

- Aprendizagem a partir de dados
    - Consiste em duas fases principais:
        - Aprendizagem ou estimação de dependências desconhecidas no sistema
        - Utilização das dependências estimadas para prever novas saídas para futuros valores de entrada no sistema
    - Estas duas fases correspondem aos dois tipos clássicos de inferência
        - Indução
        - Dedução

- Tipos de inferência
    Dados de Treino(Data Set) - Indução →  Estimação de Dependencias (Modelo) - Dedução → Predição de Saidas

- Tipos de inferência
    - A estimação de um único modelo implica a aprendizagem de uma função global para todos os valores de entrada
        - Problema, quando apenas é necessário deduzir estimativas para alguns valores
    - A estimação da função para alguns pontos de interesse sem construir um modelo global designa-se por transdução
        - Neste caso a estimação local é mais importante que a estimação global

- Estimação de modelos
    - O processo de estimação de modelos pode ser descrito, formalizado e implementado utilizando diferentes métodos ou técnicas de aprendizagem
    - Um método de aprendizagem é um algoritmo que estima um mapeamento desconhecido (dependência) entre as entradas de um sistema e as saídas, a partir de um conjunto de dados
    - Após essa dependência ser estimada com precisão, pode ser utilizada para prever saídas futuras do sistema, a partir de valores de entrada conhecidos

#### 👉 Tipos de Aprendizagem

- Aprendizagem indutiva
    - Os dois tipos mais comuns são:
        - Aprendizagem supervisionada
            - Analogia: aprendizagem com um professor
        - Aprendizagem não supervisionada
            - Analogia: aprendizagem sem um professor

- Aprendizagem supervisionada
    - Utilizada para estimar uma dependência a partir dos dados de entrada/saída
    - Os problemas de predição (classificação e de regressão) são suportados por este tipo de aprendizagem
    - O termo “supervisionada” significa que os valores de saída das amostras de treino são conhecidos
    - A saída é a característica objetivo (label, class, etc.)

- Aprendizagem não supervisionada
    - Utilizada para estimar uma dependência a partir dos dados de entrada
    - O termo “não supervisionada” significa que apenas os valores de entrada das amostras de treino são fornecidos ao sistema de aprendizagem
    - Os valores da saída são desconhecidos durante o processo de aprendizagem
        - Não existe uma característica objetivo

#### 👉 Tarefas de Aprendizagem

- Aprendizagem supervisionada
    - Classificação
        - Aprendizagem de uma função que classifica uma amostra numa de várias classes pré-definidas
    - Regressão
        - Aprendizagem de uma função que mapeia uma amostra num valor de predição numérico (real)

- Aprendizagem não supervisionada
    - Clustering
        - Tarefa em que se procura identificar um conjunto finito de agrupamentos ou clusters que descrevam os dados
    - Sumarização
        - Procura-se encontrar uma descrição compacta para um conjunto (ou subconjunto) de dados
    - Modelação de dependências
        - Tarefa em que se procura descobrir dependências entre características ou entre valores
    - Deteção de desvios e alterações
        - Tarefa em que se pretende descobrir alterações significativas num conjunto de dados (e.g., deteção de outliers)

#### 👉 Técnicas de Aprendizagem
- Métodos estatísticos
- Árvores e regras de decisão
- Redes neuronais (deep learning)
- Máquinas de suporte de vetores
- Regras de associação
- Análise de clusters
- Sistemas difusos

#### 👉 Estimação de Modelos

- Como verificar e validar um modelo?
    - Validação de modelos
        - Tem a ver com o modelo apresentar uma precisão satisfatória, consistente com os objetivos definidos
    - Verificação de modelos
        - Tem a ver com a construção do modelo de forma correta
    - A validação de modelos é uma condição necessária, mas não suficiente, para a credibilidade e aceitação de resultados

- Como dividir o conjunto de dados?
    - Dados de treino e dados de teste
    - Conjunto de dados de treino pequeno
        - Modelo resultante pouco robusto
        - Fraca capacidade de generalização
    - Conjunto de dados de teste pequeno
        - Confiança no erro de generalização baixa
        - Vários métodos para estimar o erro
    - Não há linhas de orientação que digam como dividir o conjunto de dados
    - Métodos mais comuns para dividir o conjunto de dados e estimar o erro de generalização
    - Métodos baseados no conceito de reamostragem (resampling):
        - ➡ Resubstituição
            - Método mais simples
            - Todas as amostras disponíveis são utilizadas para treino e para teste dos modelos
            - O conjunto de dados de treino e o conjunto de dados de teste são o mesmo
            - Método pouco utilizado em problemas reais de data mining
        - ➡ Holdout
            - Metade dos dados, ou dois terços, ou então três quartos, são utilizados para treino e os restantes para teste
            - Os dados de treino e os dados de teste são independentes pelo que o erro de generalização é pessimista
            - A repetição do processo, com diferentes dados de treino e de teste, selecionados aleatoriamente, permite melhorar o modelo estimado (validação estatística)
        - ➡ Leave-one-out
            - Um modelo é construído utilizando n-1 amostras para treino e avaliado com as restantes amostras
            - Este processo é repetido n vezes com diferentes conjuntos de treino de tamanho n-1
            - Esta abordagem exige muitos recursos computacionais pois é necessário construir e comparar muitos modelos
        - ➡ Cross-validation
            - Compromisso entre o método holdout e o método leave-one-out
            - Dividem-se os dados em P subconjuntos do mesmo tamanho, 1≤P≤n
            - P-1 subconjuntos são utilizados para treino e os restantes subconjuntos são utilizados para teste
            - Método mais utilizado em problemas reais de data mining
        - ➡ Bootstrap
            - Na sua forma mais simples, em vez de se analisar repetidamente subconjuntos dos dados, analisam-se subamostragens aleatórias com substituição da amostra completa
            - Permite gerar conjuntos de dados “falsos”, com o mesmo tamanho do conjunto de dados original
            - Este método é útil para conjuntos de dados com poucas amostras

#### 🤔 Data Set Inicial:

- Dados de Treino:
    - Construção do Modelo
- Dados de Teste:
    - Avaliação

#### 👉 Avaliação de Modelos

- A maioria dos modelos é avaliada com base em critérios do erro
- Erro de treino
- Erro de generalização
- Métricas para medição do erro, para problemas de regressão
- MSE (Mean Square Error)
- RMSE (Root Mean Square Error)
- NDEI (Non-Dimensional Error Index)

- Avaliação de hipóteses
    - VP: amostras classificadas como positivas e que na realidade são positivas
    - VN: amostras classificadas como negativas e que na realidade são negativas
    - FP: amostras classificadas como positivas e que na realidade são negativas
    - FN: amostras classificadas como negativas e que na realidade são positivas
