## AULA::6 - Métodos Estatícos
## Data: 04-04-2017

#### 👉 Sumário

- Introdução
- Medidas estatísticas
- Inferência Bayesiana
- Regressão linear
- Regressão logística
- Séries temporais

#### 👉 Introdução

- Estatística
    - Ciência que se dedica à recolha e organização de dados, permitindo tirar conclusões a partir de conjuntos dedados
    - Estatística descritiva
        - Relacionada com a organização e descrição de características de conjuntos de dados
    - Estatística de inferência
    - Relacionada com as conclusões que é possível tirar a partir dos dados

- Estatística de inferência
    - População: o conjunto de observações que vai ser alvo de análise estatística
        - Algo com interesse estatístico, quer seja um grupo de pessoas, de objetos, eventos, etc.
    - Tamanho da população: número de observações na população
    - Populações podem ser finitas ou (consideradas) infinitas

    - O objetivo é chegar a conclusões a respeito de uma população, quando é impossível ou não é prático observar toda a população

#### 👉 Medidas Estatísticas

- No data mining é útil perceber o que caracteriza um conjunto de dados
- Medidas estatísticas para os valores de uma única característica ou atributo
    - Localização dos dados
        - Média
        - Mediana
        - Moda
    - Dispersão dos dados
        - Intervalo
        - Variância
        - Desvio padrão
    - Outras medidas
        - Matriz de covariância (0...1)
            - Permite medir se duas características numéricas variam em simultâneo
            - Depende da magnitude das características
        - Matriz de correlação
            - Permite medir se duas características estão relacionadas entre si
            - Não depende da influência da escala
            - Indicação mais clara da força da relação linear entre duas características

#### 👉 Inferência Bayesiana

- Permite incorporar informação externa no processo de análise de dados
- Exemplo:
    - 1000 amostras:
        - [800 - Spam] P(s) = 0.8
        - [200 - Ñ Spam] P(ñs) = 0.2

#### 👉 Regressão Linear (apenas para atributos númericos)

-  Introdução
    - Técnica oriunda da estatística
    - O objetivo da análise de regressão é determinar o melhor modelo que relacione a variável de saída (Y) com várias variáveis de entrada.

- Motivação
    - O cálculo ou a medida da saída é “dispendioso”, mas a medida das entradas é acessível
    - Os valores da saída só estão disponíveis mais tarde no tempo
    - Controlando as variáveis de entrada podemos controlar as de saída
    - Identificação das dependências entre as entradas e a saída

- Regressão logística
    - Permite modelar a resposta de variáveis categóricas
    - Em vez de se estimar o valor da variável dependente, o que é estimado é a probabilidade de a variável dependente assumir um determinado valor
    - A regressão logística deve ser utilizada quando a variável de saída do modelo é uma variável categórica binária

- Séries temporais univariáveis
    - Conjunto de observações de uma só variável, recolhidas sequencialmente em intervalos de tempo idênticos

- Os dados são fornecidos apenas numa coluna, a variável observada
- Neste caso é preciso definir:
    - A saída a estimar, num instante futuro
    - Os regressores, ou seja, a informação do passado que vai ser utilizada para estimar o valor da saída num instante futuro
