## AULA::9 - Avaliação e Seleção de Modelos
### Data: 09-05-2017

#### 👉 Algoritmos de Data Minig

- Aprendizagem supervidionada
    - Métedos Estatisticos
    - Árvores e regras de decisão
    - Memory-Based Learning
- Aprendizagem não supervidionada
    - Associação
    - Clustering

#### 👉 Sumário

- Introdução
    - Métricas para problemas de regressão
    - Métricas para problemas de classificação
        - Gráficos de Ganho
        - Curvas ROC
        - Curvas de Precisão/Recall
    - Aprendizagem com custos
    - Seleção de modelos

- Avaliação de Modelos
    - A avaliação de modelos é um dos aspetos chave no processo de data mining
    - A maioria dos modelos é avaliada com base em critérios do erro
    - Construção do modelo
    - Erro de treino
    - Avaliação do modelo
    - Erro de teste ou erro de generalização : O modelo é confrontado com dados que nuca viu.
    - O erro de treino nunca é um bom indicador de desempenho dos modelos
    - A avaliação deve ser feita com dados diferentes dos que foram utilizados na construção dos modelos
    - Separação dos dados de treino e teste
    - A avaliação dos modelos pode ser feita utilizando um único critério ou múltiplos critérios

#### 👉 Problemas de Regressão

- Critérios do erro
    - O erro associado a uma amostra pode assumir qualquer valor
    - Algumas métricas para medição do erro
        - MSE (Mean Square Error)
        - RMSE (Root Mean Square Error)
        - NDEI (Non-Dimensional Error Index)
    - Que medida do erro?
        - Nalguns casos o melhor é considerar todas as medidas, noutros é irrelevante

(Erro = Saida Real - Previsão do Modelo)

#### 👉 Problemas de Classificação

- Critérios do erro
- Cada amostra classificada correta ou incorretamente
- Métricas globais
    - Taxa de acerto
    - Taxa de erro
- As métricas globais poderão não ser as mais adequadas para avaliar os modelos
    - Problemas não balanceados
    - Data sets com poucas amostras

- Avaliação de Hipóteses
    - VP: amostras classificadas como positivas e que na realidade são positivas
    - VN: amostras classificadas como negativas e que na realidade são negativas
    - FP: amostras classificadas como positivas e que na realidade são negativas
    - FN: amostras classificadas como negativase que na realidade são positivas

#### 👉 Gráficos de Ganho

- Paradigma do marketing direto
    - Fazer uma prospeção de clientes para  contactar
    - Não é necessário contactar todos os clientes
    - Número de clientes alvo é geralmente muito menor que o número de clientes resultantes da prospeção
    - Aplicações típicas
        - Vendas por catálogo, correio direto (e-mail)

    - Avaliação do marketing direto
        - Precisão na totalidade do conjunto de dados não é a medida correta
        - Abordagem
            - Desenvolver um modelo alvo
            - Pontuar todos os clientes resultantes da prospeção e ordená-los por ordem decrescente de pontuação
            - Selecionar os primeiros P% clientes da prospeção para a ação
    - Como decidir qual a melhor seleção?

#### 👉 Curvas ROC

- Análise de Curvas ROC
    - ROC: Receiver Operating Characteristic
    - Usado na deteção de sinais para mostrar o limiar entre a taxa de acertos e de falsos alarmes num canal de comunicação
    - As curvas ROC são idênticas aos gráficos de ganho
    - Utilizadas no contexto do data mining para avaliar o desempenho dos modelos
    - Diferenças em relação aos gráficos de ganho
    - Eixo das abcissas: percentagem do número total de amostras negativas (taxa de FP), em vez do tamanho da amostra
    - Eixo das ordenadas: percentagem do número total de amostras positivas (taxa de VP), em vez da percentagem de acertos

#### 👉 Curvas de Precisão/Recall

- Conceito de Precisão e Recall
    - Precisão: percentagem das amostras previstas para uma classe pelo modelo classificadas corretamente.
        - P = VP/VP + FP
    - Recall: percentagem das amostras reais de uma classe classificadas corretamente pelo modelo.
        - R = VP/VP + FN

#### 👉 Seleção de Modelos
- Princípio MDL
    - MDL: Minimum Description Length
    - É definido da seguinte forma:
        - Espaço necessário para descrever uma teoria
        - Espaço necessário para descrever os erros da teoria
    - No nosso caso a teoria é o modelo e os erros são os erros nos dados de treino
    - Princípio MDL é um critério de seleção de Critérios para seleção de modelos

- Os critérios de seleção de modelos
    - Os critérios de seleção de modelos procuram encontrar um equilíbrio entre:
        A. Complexidade do modelo
        B. Precisão do modelo
    - Raciocínio: um bom modelo é um modelo simples que permita obter boa precisão para o conjunto de dados
    - Occam’s Razor: a melhor teoria é a mais simples, que descreva todos os factos modelos.

- Complexidade vs. Precisão
    - **Teoria 1:** Teoria muito simples, elegante e que explica quase na perfeição os dados
    - **Teoria 2:** Teoria muito mais complexa, mas que permite reproduzir os dados sem erros
    - A Teoria 1 é provavelmente a preferida
    - O mesmo raciocínio deve ser aplicado n seleção de modelos

- Resumo
    - Nunca calcular as métricas de avaliação de modelos nos dados de treino
    - Evitar o fenómeno de overfitting (sobre ajustamento do modelo aos dados de treino)
    - Utilizar validação cruzada (crossvalidation), sobretudo para conjuntos de dados pequenos
    - Equacionar custos associados à aprendizagem sempre que for apropriado


