## AULA::7 - Árvores e Regras de Decisão
## Data: 18-04-2017

#### 👉 Sumário

- Árvores de decisão
    - Algoritmo ID3
    - Algoritmo C4.5
    - Vantagens e desvantagens
- Regras de decisão
    - Algoritmo 1R
    - Algoritmo PRISM

#### 👉 Introdução

- São usadas em muitas aplicações reais, principalmente em problemas de classificação
- Objetivo: Criar um modelo de classificação (Classificador) que, com base nos atributos disponíveis, prevê a classe da nova amostra
- Existem muitos algoritmos para induzir árvores de decisão
    - ID3
    - C4.5 (J48, no Weka)
    - CART, etc.
- A maioria dos algoritmos adota uma abordagem top-down, procurando apenas numa parte do espaço de pesquisa

#### 👉 Composição de uma árvore

- Nodos ou nós
    - Onde são testados os atributos
- Ramos
    - Saem dos nodos e representam os valores que os atributos podem tomar, ou seja, o resultado de um teste
- Folhas
    - Representam as etiquetas correspondentes às classes

#### 👉 Algoritmo ID3

- Descrição do algoritmo
    - Começa com todos os exemplos no nodo raiz, sendo então escolhido um atributo para particionar as amostras
    - A cada uma das partes das amostras é aplicado o método inicial, ou seja, a cada um dos nodos filhos é aplicado de forma recursiva o mesmo procedimento
    - Cada caminho na árvore representa uma regra de decisão

- Escolha de atributos
    - Ponto crítico do algoritmo
    - Para gerar a árvore de decisão o algoritmo usa os conceitos de entropia e ganho de informação
    - Estes conceitos derivam da teoria da informação
        - Claude Shannon (1916-2001)
        - Pai da teoria da informação
    - Minimização da entropia: minimizar o número de testes que permitem classificar uma amostra
    - Maximização do ganho de informação: consiste em minimizar a informação necessária para classificar uma amostra na subárvore resultante

- Escolha de atributos
    - Minimizar o número de testes para a classificação de uma amostra no conjunto de dados
    - A seleção do atributo para um nodo é baseada na assunção de que a complexidade da árvore está relacionada com a quantidade de informação contida no valor de determinado atributo, ou seja, maximizar o ganho de informação

ex: Se Céu = Chuva e Vento = Não Então Jogar Ténis = P.

- Regras de decisão
    - Cada folha da árvore indica uma classe
    - Um nó de decisão da árvore corresponde a um teste de um atributo
    - As ramificações do nó correspondem a todos os resultados possíveis do teste
    - Cada caminho desde a raiz até uma folha da árvore representa uma regra de decisão

#### 👉 Algoritmo C4.5

- O ID3 dá preferência a árvores com um grande fator de ramificação
- Se cada ramo conduzir a uma folha com um único atributo, a entropia resultante é zero
- O C4.5 usa a razão entre o ganho e a entropia dos conjuntos de amostras resultantes.

- Algumas considerações
    - Permite lidar com atributos com valores em falta e gerar de forma automática as regras de decisão
    - Permite lidar com atributos numéricos
    - É feita a poda da árvore, sendo retirados os ramos que não contribuem para a redução do erro de classificação

- Conclusão
    - Na geração das regras de decisão é feita generalização
    - O conjunto de regras é mais compacto
    - O processo implica uma ordenação das regras resultantes sendo necessário definir uma classe à qual são atribuídas as amostras não classificados pelas regras resultantes
    - Regra ou classe por omissão

#### 👉 Árvores de Decisão

- Vantagens
    - Capazes de gerar regras percetíveis
        - Modelos interpretáveis
    - Lidam tanto com valores contínuos como com valores simbólicos
    - Indicam claramente os atributos mais importantes para a previsão
    - “Leves” do ponto de vista computacional, sobretudo em utilização

- Desvantagens
    - Problemas com muitas classes ou valores contínuos
    - Problemas com regiões não retangulares no espaço de dados.

#### 👉 Regras de Decisão

- Introdução
    - Induzir um conjunto de regras a partir dos dados
    - Aplicar essas regras para determinar a classe da nova amostra
    - Algoritmos para indução de regras
    - 1R
    - PRISM (Gera regras perfeitas)

- Conceitos básicos
    - Cobertura: % de amostras que satisfazem o antecedente da regra
    - Precisão: % das amostras cobertas pela regra que satisfazem o antecedente e o consequente
    - Exemplo
        - Uma regra de decisão pode cobrir 10 das 50 amostras, das quais 8 são corretas
        - Cobertura = 20%
        - Precisão = 80%
    - Apresentação das regras
        - Conjunto: a ordem pela qual as regras são avaliadas é irrelevante
        - Lista ordenada: a ordem neste caso é relevante
    - Se todas as amostras são cobertas por pelo menos uma regra de decisão então o conjunto/lista ordenada é considerado exaustivo

- Algoritmos
    - Duas abordagens principais
        - Cobertura: Em cada etapa é encontrada uma regra que cobre algumas amostras
            - Exemplo: Algoritmo 1R
        - Separar e conquistar: escolher a regra que identifica corretamente mais amostras, separá-las, e de seguida repetir o processo
            - Não confundir com a abordagem dividir e conquistar
                Assim que classificadas são separadas exemplo:
                 - Temos 100 amostras classficamos 10, então essas 10 saem ficam 90 amostras...
            - Exemplo: Algoritmo PRISM - Regras perfeitas (classificadas corretamentes)

#### 👉 Algoritmo 1R

- Construir uma regra para cada combinação atributo/valor de modo a predizer a classe mais comum
- Escolher o atributo com menor taxa de erro
- A designação 1R (One-Rule) advém do facto de todas as regras testarem apenas um único atributo
- Algoritmo muito simples (Holte, 1993)

#### 👉 Algoritmo PRISM

- Construir uma regra e de seguida remover as amostras cobertas pela regra antes de procurar uma nova regra
- Começar com uma regra com elevada taxa de cobertura e melhorar a precisão adicionando mais condições
- O objetivo é maximizar a precisão de cada regra
- O algoritmo gera regras perfeitas

#### 👉 Regras de Decisão

- Aplicação das regras a dados de teste
    - Três situações distintas
    - Nenhuma regra dispara
    - Apenas dispara uma regra
- Dispara mais de uma regra
- No primeiro caso criar uma regra por omissão (classe mais frequente)
- As situações de conflito surgem quando dispara mais do que uma regra
- Ordenar as regras por prevalência ou qualquer critério de mérito

- Exercício
    - Número de amostras? *27*
    - Número de classes? *2*
    - Número de regras? *4*
    - Taxa de acerto? *24/27 24 porque são 3( que estão mal classificadas) - 27*

