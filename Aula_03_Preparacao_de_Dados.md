## AULA 3 - Reparação dos Dados
### Data: 14-03-2017

#### 👉 Sumário

- Preparação dos Dados:
- Organização dos dados
- Transformação dos dados
- Dados em falta
- Outliers
- Séries temporais
- Outros tipos de dados

#### 👉 Introdução

- Preparação dos dados
    - Um dos passos mais críticos no processo de data mining é a preparação do conjunto inicial de dados
    - Muitas vezes negligenciada no ensino e investigação, em detrimento da etapa de modelização
    - No mundo real a situação é inversa, pois há mais esforço na preparação de dados do que na modelização

- Dados em bruto (Raw data sets)
    - Problemas típicos
        - Atributos obsoletos
        - Atributos redundantes
        - Valores em falta
        - Erros de inserção
        - Outliers
        - Valores inconsistentes
        - Dados numa forma não adequada para os algoritmos de data mining

- Objetivos
    - Organizar os dados numa forma standard pronta para processamento por algoritmos de data mining
    - Preparar as características que levem ao melhor desempenho do modelo, o que geralmente implica a  transformação dos dados
    - A preparação dos dados exige muito trabalho humano

#### 👉 Organização dos Dados

- Forma standard
    - A capacidade dos computadores analizarem dados é limitada pela descrição inicial deles.
    - Os métodos de aprendizagem podem beneficiar muito de algum conhecimento adicional introduzido na preparação dos dados
    - Derivação de novas características
    - Características têm de ser codificadas numericamente ou com códigos
    - Deve ser especificado qual a característica objetivo
    - Objetivo claro

- Medidas standard
    - Numéricas
    - Categóricas ou nominais
        - Possui dois ou mais valores
        - Não possuem uma relação de ordem ou distância
        - Exemplo: Cor dos olhos
    - Periódicas
        - Valores repetem-se (Exemplo: Dias da semana)
    - Contínuas ou quantitativas
        - Número infinito de valores (Exemplo: Temperatura=
    - Discretas ou qualitativas
        - Número finito de valores (Exemplo: Género, Estado Civil)

### Transformação dos Dados
- Necessidade
    - Os dados originais precisam de ser transformados para poderem ser utilizados pelos algoritmos de data mining
    - Transformações
        - Não são ótimas e São feitas muitas vezes manualmente
    - Na transformação de dados a experiência e saber humanos são fundamentais

- Tarefas principais
    - Seleção de características (feature selection )
        - Aprofundado no próximo capítulo
    - Composição de características (feature composition)
        - Normalização
        - Smoothing
        - Diferenças e rácios
        - Funções de agregação

- Normalização
    - Escala decimal (multiplicar ou dividir por 10k), mantém os valores em [-1;1]
    - Min-max
    - Desvio padrão

- Smoothing
    - Pode haver demasiado pormenor nos dados que o algoritmo de data  mining não vai aproveitar
    - Forma de redução dos dados
    - Existem métodos mais elaborados

- Diferenças e rácios
    - Os métodos de aprendizagem podem beneficiar muito de algum  conhecimento adicional introduzido na preparação dos dados
    - Especificação de uma diferença, conduz a um menor número de  alternativas
    - Especificação de um rácio, que permita separar parte das amostras
    - Podem ser utilizadas para as variáveis independentes ou dependentes

- Funções de agregação
    - Forma muito comum para a derivação de novas características ou  atributos
    - Exemplos
        - Contagem
        - Soma
        - Média, desvio padrão
        - Máximo, mínimo
        - Moda
        - Mediana

### Dados em Falta (Como resolver o problema?)
- Se faltar a label (objetivo) normalmente o caso/amostra é descartado
- Um valor em falta é um problema porque não pode ser comparado
    - Solução: substituir o valor em falta (?) por
        - Constante
        - Média da característica
        - Média da classe
- Soluções tentadoras, mas o valor atribuído nunca será o valor correto

### Outliers
**Descrição:** São amostras inconsistentes com o comportamento geral dos dados.

- Podem ser causados por um erro de medida/inserção de dados
- Podem ser resultado de uma variação espúria nos dados
- Exemplos:
    - Idade de uma pessoa: -1 (erro!!)
    - Número de filhos: 25
    - Pode ser verdadeiro e representar uma variação inerente nos dados

É necessário cuidado a determinar outliers, pois se uma amostra considerada incorrectamente como outlier for removida pode comprometer o desempanho do modelo.

### Séries Temporais

- Séries temporais explícitas
    - A dimensão tempo é representada explicitamente através de um ou vários atributos (e.g., Data Warehouse)
- Séries temporais implícitas
    - A dimensão tempo está inerente aos dados
- Séries temporais univariáveis
    - Os dados são fornecidos apenas numa coluna, a variável observada

- A variável física de entrada e saída pode inclusive ser a mesma, mas em instantes temporais distintos
- Forma standard mais uma dimensão, que é a dimensão tempo
- Os dados são sequenciais, medidos ao longo do tempo
- Requerem formas de preparação e transformação especiais
- A ordem dos dados tem de ser preservada!!!
