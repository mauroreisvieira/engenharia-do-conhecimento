## AULA 4 - Redução dos Dados
### Data: 21-03-2017

#### 👉 Sumário

- Redução dos Dados:
- Tipos de redução de dados
- Redução de características
    - Seleção de características
    - Composição de características
- Redução de amostras
- Redução de valores

#### 👉 Introdução

- Para conjuntos de dados de grande dimensão  é necessario uma redução de dados antes de aplicar as tecnincas de data mining.
- Geralmente os modelos são construidos a partir de um subconjunto de caracteristicas e amostras.

**Objetivos Principais:**
- Preservar os dados originais, eliminando dados não essenciais:
    - Dados com ruído ou contaminados
    - Dados correlacionados
    - Dados redundantes
    - Suavizando características (tirando detalhe)
- Escolher um subconjunto de dados relevante para o problema de modo a garantir:
    - Máximo desempenho com o mínimo de dados e esforço de processamento

A redução de dados deve envolver os seguintes parâmetros de análise:
    - Tempo computacional
        - Aprender mais rápido
    - Precisão do modelo
        - Aprender com mais precisão
    - Representação do modelo
        - Menor complexidade
Na maioria das situações não é possível melhorar em simultâneo os parâmetros

- ⬇ Tempo Computacional
- ⬆ Precisão do Modelo
- ⬇ Complexidade do Modelo

#### 👉 Tipos de Redução

- Redução de Dados
    - Redução de características
        - Eliminar colunas (características ou atributos) no conjunto de dados inicial
- Redução de casos ou amostras
    - Eliminar linhas (amostras ou registos) no conjunto de dados inicial
- Redução de valores
    - Eliminar valores para determinada(s) característica(s)

#### 👉 Redução de Características

- O processo de redução de características deverá permitir obter:
    - Menos dados, para que a aprendizagem do algoritmo seja mais rápida
    - Melhor precisão, para que o modelo tenha maior capacidade de generalização
    - Resultados mais simples, para que seja mais fácil a sua compreensão e utilização
    - Menos características, para que na próxima recolha de dados características redundantes ou irrelevantes sejam descartadas.

**Duas abordagens principais**
- Seleção de características
    - Encontrar um subconjunto de características que tenha um desempenho comparável ao conjunto total.
- Composição de características
    - Substituir o conjunto inicial de características por outras resultantes da composição.
        - Peso e Altura → Índice de Massa Corporal (IMC)
        - Recência, Frequência e Monetário → Índice RFM

#### 👉 Seleção de Características

Depende daquilo que é avaliado e do método de avaliação
- O que é avaliado?
    - Características individuais ou um subconjunto de características
- Método de avaliação
    - Independente
    - Algoritmo de aprendizagem, o que implica a construção de um modelo

#### 📌 Métodos de Avaliação Independente

- Algoritmos de ranking de características
    - Tipicamente quando cada característica é avaliada individualmente
    - As características são ordenadas de acordo com um critério de avaliação
    - Os algoritmos apenas indicam a relevância de uma característica em comparação com as restantes
    - Selecionar quantas características usar fica ao critério do especialista de data mining

- Algoritmos de subconjuntos mínimos de características
    - Com base num critério de avaliação devolvem um subconjunto em que não é feita uma distinção entre características
    - Todas têm o mesmo ranking
    - As que fazem parte do subconjunto são relevantes para o processo de data mining
    - As outras características são consideradas irrelevantes

- Critérios de Seriação/Avaliação
    - Consistência dos dados disponíveis
    - Conteúdo de informação
        - Ganho de informação
    - Distância entre amostras
    - Dependência estatística entre as características
        - Correlação elevada com a característica objetivo
        - Correlação baixa com as outras características

- Critério de Avaliação: Correlação
    - Medida estatística da relação entre características de um data set
    - Apenas para características numéricas
    - Valores de zero, ou próximo de zero, indicam que não existe correlação

- Correlação positiva
    - Quando ambas as características se movem na mesma direção
- Correlação negativa
    - Quando as características se movem em direções opostas
- Não confundir correlação com causalidade

#### 📌 Método de Avaliação com algoritmos de Aprendizagem

- Necessária a presença de algoritmos de aprendizagem
- Avaliar subconjuntos de características
- Utilizar os mesmos métodos para selecionar subconjuntos de características
- Aplicar a cada algoritmo de aprendizagem
- Devolver o melhor subconjunto de características

- Passos típicos
    - Selecionar subconjunto de características
    - Construir modelo com base no subconjunto de características
    - Avaliar o resultado do modelo
    - Repetir os passos anteriores para diferentes subconjuntos de características e algoritmos de aprendizagem até obter o melhor subconjunto de características


#### 👉 Redução de Amostras (Linhas)

- A redução de casos ou amostras é a tarefa mais complexa na redução de dados
- Necessária, se as ferramentas ou os algoritmos de data mining não suportarem a carga
- Amostras sistemáticas ou aleatórias
- O tamanho do conjunto de dados depende do número de características e da complexidade do problema

- Técnicas de amostragem
    - Amostragem sistemática
        - Escolha baseado num criterio (exemplo: todas as amostrares pares, ou as primeiras 50.000...)
    - Amostragem aleatória
        - Cada amostra do conjunto de dados inicial tem a mesma probabilidade de ser selecionada para o subconjunto
    - Amostragem aleatória sem substituição
        - Não há amostras repetidas
    - Amostragem aleatória com substituição

#### 👉 Redução de Valores

- A redução de valores de uma característica é geralmente baseada em técnicas de discretização
- Discretizar os valores contínuos de uma característica num número pequeno de intervalos
- Cada intervalo corresponde a um símbolo discreto
