## AULA::8 - Memory Based Learning
## Data: 11-04-2017

#### 👉 Sumário
- Introdução
- Algoritmo K-NN
- Função de distância
- Número de vizinhos
- Função de combinação
- Vantagens e desvantagens dos algoritmos de Memory-Based Learning

#### 👉 Introdução
- Memory-Based Learning
    - Entradas semelhantes mapeiam saídas semelhantes
        - Se não for verdade a aprendizagem é impossível
        - Se for verdade a aprendizagem reduz-se à definição do conceito de “semelhante”
    - Outras designações
        - Instance–Based Learning
        - Case-Based Learning
        - Kernel-Based Learning
    - Faz uso direto dos casos do conjunto de dados para chegar aos resultados
    - Considera que a resposta a um novo problema é semelhante à resposta dada a problemas parecidos
    - Aplicações
        - Deteção de fraudes
        - Previsão da resposta de clientes
        - Tratamentos médicos
        - Classificação de texto

**Quatro aspetos essenciais neste método de aprendizagem:**

- Função de distância para medir a semelhança
    - Distância Euclidiana
- Número de vizinhos considerados
    - Pelo menos um
- Função de pesos (opcional)
- Função de combinação
    - Prever a mesma saída que a dos K vizinhos mais próximos

- Para esta técnica ter sucesso a base de casos deverá conter aproximadamente o mesmo número de casos de cada classe
- Para cada classe deverá conter no mínimo 20 casos (amostras)
- Necessário definir
    - Função de distância
    - Número de vizinhos
    - Função de combinação

#### 👉 Algoritmo k-NN
- K-Nearest Neighbors
- O algoritmo k-NN é um método de aprendizagem não paramétrico
- Não é criado um modelo
- Todos os casos de treino são armazenados
- Aprendizagem por analogia
- Sabedoria popular: “Diz-me com quem andas, dir-te-ei quem és …”
- Utilizado sobretudo em problemas de classificação, podendo também ser utilizado em problemas de regressão

#### 👉 Memory-Based Learning

**Desvantagens:**

- Computacionalmente pesado, pois demora muito tempo para cada novo exemplo
- Exige grande espaço de armazenamento para o conjunto de dados de treino
- Resultados dependentes da escolha da função de distância, função de combinação e número de vizinhos
- Atributos não numéricos



