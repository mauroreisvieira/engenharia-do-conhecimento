## AULA 1 - Introdução ao Data Mining
### Data: 14-03-2017

#### 👉 Sumário

- Conceito
    - Dados, Muitos Dados
    - Origens do Data Mining
    - Problemas de Data Mining
    - Aplicações de Data Mining
    - Projetos de Data Mining
    - Processo de Data Mining

#### 👉 Introdução

- O que é o Data Mining?
    - É o processo de extração de informação e conhecimento relevante a partir de grandes quantidades de dados

- Muitos Dados
- Tamanho dos dados depende
    - Número de casos/amostras
    - Número de características/atributos
    - Número de valores diferentes para cada característica/atributo

- Principais Problema de data mining
    - Classificação
    - Regressão
    - Clustering
    - Sumarização
    - Modelação de dependências
    - Deteção de alterações e desvios

- Problemas típicos (1)
    - Falta de visão
        - Não saber que informação se pretende retirar dos dados
    - Dados desatualizados
        - Dados em falta ou incorretos
    - Lutas entre departamentos
        - Falta de disponibilização de dados
        - Fraca cooperação do departamento de processamentos online

    - Restrições legais de privacidade
    - Dificuldades técnicas de ligar os dados
        - Formatos variados
    - Atraso da informação
        - A informação disponível está desfasada da realidade atual
    - Interpretação
        - Não existe documentação de suporte para os dados disponibilizados que permita a sua interpretação

#### 👉  Metodologias

- Principais metodologias para o processo de data mining
    - PRMA (Weiss & Indurkhya)
    - SCECMR (Usama Fayyad)
    - SEMMA (SAS Institute)
    - CRISP-DM (SPSS-IBM)

#### CRISP-DM: CRoss-Industry Standard Process for Data Mining
- Principais características
    - Não proprietária
    - Neutral
        Indústria
        Aplicações
        Ferramentas
    - Guia de projeto
    - Foco no negócio
    - Base na experiência

- Etapas
    - Compreensão do negócio
    - Compreensão dos dados
    - Preparação dos dados
    - Modelização
    - Avaliação
    - Colocação em produção

## Etapas CRISP-DM
### Compreensão do negócio
- Tarefas
    - Definir os objetivos de negócio
        - Diminuir as devoluções em 2%
        - Aumentar as vendas em 5%
        - Diminuir os custos de operação em 100 K€
    - Avaliar o cenário
    - Definir os objetivos de data mining
        - Taxa de acerto superior a 70%
        - AUC mínimo de 0,95
    - Elaborar o plano de projeto

- Principais resultados
    - Descrição do negócio, objetivos e critérios de sucesso
    - Inventário de recursos, requisitos, assunções e restrições
    - Riscos, contingências, custos e benefícios
    - Determinar objetivos de data mining e critérios de sucesso
    - Plano de projeto e Metodologia
    - Escolha inicial de ferramentas e técnicas

### Compreensão dos Dados
- Tarefas
    - Recolha dos dados iniciais
    - Descrição dos dados
    - Exploração dos dados
    - Verificação da qualidade dos dados

- Principais resultados
    - Documentar como foram obtidos os dados
    - Documentar a descrição dos dados iniciais
    - Documentar as tarefas de exploração dos dados iniciais
    - Documentar a qualidade dos dados iniciais

### Preparação de Dados
- Tarefas genéricas
    - Seleção dos dados
    - Limpeza dos dados
    - Derivar novos dados
    - Integração dos dados
    - Formatação dos dados
    - Criação do Data Set

- Principais resultados
    - Documentar os motivos de seleção/exclusão de dados
    - Documentar as tarefas de limpeza efetuadas
    - Derivar atributos e gerar registos
    - Junção de dados adicionais
    - Formatar os novos dados
    - Descrição do data set

### Modelização
- Tarefas genéricas
    - Escolher as técnicas de modelização
    - Definição do plano de testes
    - Construção dos modelos
    - Avaliação dos modelos

- Principais resultados
    - Técnicas selecionadas
    - Assunções de modelização
    - Separação dos dados em conjuntos de treino e teste/validação
    - Valores de parametrização
    - Modelos obtidos
    - Descrição dos modelos
    - Avaliação do modelo obtido e afinação de parâmetros

### Avaliação
- Tarefas genéricas
    - Avaliação dos resultados
    - Revisão do processo
    - Determinar ações futuras

- Principais resultados
    - Avaliação dos resultados tendo em conta os critérios de sucesso definidos
    - Modelos escolhidos
    - Revisão do processo
    - Recomendação de implementação do modelo, ou redefinição do problema

### Colocação em Produção
- Tarefas genéricas
    - Planeamento da colocação em produção
    - Planeamento da monitorização e manutenção
    - Elaboração do relatório final
    - Revisão do projeto

- Principais resultados
    - Plano de colocação em produção
    - Plano de monitorização e manutenção
    - Relatório final
    - Apresentação final
    - Documento com a experiência adquirida no projeto

## Aspetos comuns (SABER)

- O processo de data mining é iterativo
- Existe realimentação ao longo do processo
- O processo de data mining continua após uma solução ser colocada em produção
