## AULA::10 - Regras de Associação
### Data: 16-05-2017

#### 👉 Actividades Analíticas

**Espetro das Atividades Analíticas**

 - __Descritivo:__ O que aconteceu?
 - __Diagnistico:__ Por que é que aconteceu?
 - __Preditivo:__ O que é que vai acontecer?
 - __Proscritivo:__ Como fazer para que acontença?

**Espetro das Atividades Analiticas**

  - Modelos preditivos
  - Permite a previsão de valores desconhecidos com base em dados conhecidos.
  - Problemas/Tarefas de data mining.
      - Classificação
      - Regressão
      - Series temporais
  - Aprendizagem supervisionada.

  - Modelos descritivos
  - Permite a identificação de padrões nos dados
  - Problemas/Tarefas de data mining.
      - Clustering
      - Classificação
      - Modelo de dependências
      - Deteção de desvios e outliers
      - Deteção de alterações
  - Aprendizagem não supervisionada

**Regras de Associação**

  - Forma mais comum de descobrir padrões locais em sistemas não supervisionados.
      - Modelação de dependências
  - O objetivo é descobrir regras interessantes desconhecidas

**Tipos de regras de associação**

  -  Regra útil
      - Indicadores semânticos validos
      - Sugere linha de ação
      - Explica de forma intuitiva

  - Regras Triviais
      - Não construí uma mais valia.
      -  Comprava a experiencia empirica

  - Regras inexplicável
      -  Não a um explicação plausível.

**Conceito de Suporte**
 - Regra de associação
      `X => Y (Quem compra X tb compra Y)`

**Conceito de Confiança**

 - A regra `X => Y` têm confiança c, se c% dos carrinhos, ou transações. que contem X também contem Y.

 - Regras com valores elevados para confiança e suporte dizem se **regras fortes**.

#### 👉 Algoritmo Apriori

 - 0001 - (A,C,D)
 - 0002 - (B,C,E)
 - 0003 - (A,B,C,E)
 - 0004 - (B,E)

 `Suporte Minimo: >= 50% `
 `Confiança Minima: >= 80% `

**Vantagens das Regras de Associação**
 - Resultados facilmente interpretaveis
 - Facil d eperceber
 - Pode produzir resultados sem saber o que se procura.

**Desvantagens das Regras de Associação**
- Crescimento exponencial da complexidade com dimensão do problema.
- Nivel de detalhe pouco defenido
- Problemas com produtos que aparecem poucas vezes.

**Web Minig**
- Algoritmos HITS e LOGSOM

