## AULA::11 - Clustering
### Data: 23-05-2017

#### 👉 Introdução

**Conceitos Basicos:**

- Cluster:
    - Coleção de objectos.
- Analise de Clusters:
    - Agrupamento de conjuntos de objectos dos dados fornecidos.
- Clustering:
    - Problema de aprendizagem não supervizionado.
    - Os padrões existentes nos dados são desconhecidos.
    - Não existe classes defenidas.

**Aplicações Típicas**

- Descobrir relações iniais nos dados.
- Etapa de pré-processamentos para outros algorimos
- Reconhecimento de padrões.
- Analise de dados geograficos.
- Processamento de imagem.
- Analise de dados economivos.

**Classificação vs Clustering**

- Classificação:
    - Aprendizagem supervizionado (existe sempre caracteristica objectiva).
    - Determina a classe corresponsdente a cada nova entrada com base em amostras previamente classificadas.
    - Sepraras amostras em classes.

- Clustering
    - Aprendizagem não supervisionada.
    - Encontrar grupos de amostras autenticos a partir de amostras não classificadas.

#### 👉 Algoritmos de Clustering

**Diferentes abordagens**

- Algoritmos de particionamento
    - K-Means; K-Medoids; CLARANS
- Algoritmos hierárquicos
    - BIRCH; CURE
- Baseados na densidade
    - DBSCAN; OPTICS; CLIQUE
- Baseados numa grelha
    - STING; WaveCluster
- Baseados num modelo
    - Abordagens da Estatística, IA, Soft Computing

**Medida de Distância**

- Para medir a semelhança entre objectos.

**Avaliação dos Clustering**

- Inpeção Manual
    - Apenas para 2D e 3D
- Benchmarking
    - Comparação de resultados.
- Medidas de qualidade de clustering
    - Medidas de distancia.
    - Similaridade intercluster (deve ser alta).
    - Similaridade intercluster (deve ser baixa).

**Algoritmos de Particionamento**

- Fazer o particionamento de um conjunto de dados D, constituído por n amostras, em k clusters
- Para o valor de k, encontrar uma partição de k clusters que otimize o critério de particionamento escolhido
    - Ótimo global: implica enumerar exaustivamente todas as partições
- Métodos heurísticos
    - Algoritmo K-Means (J. B. MacQueen, 1967)

#### 👉 Algoritmos K-Means

**Passos do Algoritmo**

- **Passo 1:** Definir o número de clusters pretendido, K
- **Passo 2:** Calcular aleatoriamente os centros iniciais ou sementes dos clusters
- **Passo 3:** Atribuir cada amostra ao centro (semente) que lhe está mais próximo
- **Passo 4:** Calcular o novo centro de cada cluster
- **Passo 5:** Repetir os passos 3 e 4 até o algoritmo convergir
    - Variação nos centros dos clusters entre duas iterações menor que um limiar pré-definido

**Observações**

- Funciona apenas com características numéricas, de preferência normalizadas
- O resultado do particionamento depende da iniciação dos protótipos
- O algoritmo pode ficar “preso” num mínimo local
- Para aumentar a probabilidade de encontrar o ótimo global deve-se correr o algoritmo várias vezes com diferentes iniciações

**Vantagens**

- Algoritmo simples e fácil de compreender
- Amostras são automaticamente imputadas aos clusters

**Desvantagens**

- É necessário definir à partida o númerode clusters, K
- Todas as amostras pertencem a um cluster, obrigatoriamente
- Demasiado sensível à presença de outliers e ruído
- Problemas quando os clusters são de diferentes tamanhos, densidades e formas

#### 👉 Clustering Difuso

**Abordagem tradicional**

- Cada amostra pertence a um e um só cluster
- O grau de pertença é exatamente 1
- Existem fronteiras bem definidas entre clusters

**Abordagem difusa**

- Permite que cada amostra pertença a mais do que um cluster, com diferentes graus de pertença
- O grau de pertença em relação a um cluster é um valor entre 0 e 1
- As fronteiras entre clusters não estão bem definidas, são difusas

**Objetivos**

- Algoritmo que particione o conjunto de dados num número ótimo de clusters
- Algoritmo que considere a variabilidade na forma dos clusters, densidade dos clusters e o número de pontos associados a cada cluster
- Os protótipos dos centros são criados através de um processo de aprendizagem não supervisionado


**Algoritmo Fuzzy C-Means**

- **Passo 1:** Escolher os protótipos iniciais ou sementes dos clusters
- **Passo 2:** Calcular o grau de pertença de cada amostra para todos os clusters
    - Somatório dos graus de pertença de cada cluster é 1
- **Passo 3:** Calcular os novos centros de cada cluster
- **Passo 4:** Repetir os passos 2 e 3 até o algoritmo convergir
    - Variação nos graus de pertença dos clusters entre duas iterações menor que um limiar pré-definido



