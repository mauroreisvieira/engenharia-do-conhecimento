## AULA 2 - Exploração de Dados
### Data: 14-03-2017

#### 👉 Sumário
- Introdução
- Excelência dos gráficos
- Representação em 1D, 2D e 3D
- Representação em 4D ou mais dimensões
    - Múltiplas vistas
    - Gráficos de dispersão
    - Coordenadas paralelas

#### 👉 Introdução
- Papel da Visualização
    - Suportar a exploração interativa de dados
    - Ajudar na representação de resultados
    - Desvantagem
        - Requer a presença do olho humano
    - Pode conduzir a equívocos

#### Visualização

**Visualização 4D ou +D**

- Coordenadas Paralelas
- Cada ponto é representado por uma linha
- Pontos similares correspondem a linhas similares
- Linhas cruzadas correspondem a atributos não relacionados
- Exploração interativa e clustering
- Problema: Limitada a aproximadamente 20 dimensões

- Gráficos de Dispersão
    - Representa cada combinação de pares de variáveis num gráfico 2D
    - Utilidade
        - Colocar em evidência correlações lineares
    - Problema
        - Não mostra efeitos multivariáveis

**Exploração de Dados**

- Ferramentas Computacionais
    - Exemplos de ferramentas que juntam à vertente analítica poderosas técnicas de exploração e visualização de dados
    - Tableau Software
        - Técnicas de visualização interativas focadas no Business Intelligence and Analytics
    - Qlik
        - Business Intelligence and VisualizationSoftware
