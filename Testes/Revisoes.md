**Algoritmo**

Algoritmo | Tipo de Aprendizagem | Tipo de Problema | Tipo de Atributos | Valores em Falta | Normalização dos Dados | Transparencia do Modelo |
--- | --- | --- | --- | --- | --- | --- |
Naive Bayes | Supervisionada | Classificação |
Regressão Linear | Supervisionada | Regressão |
Regressão Logisitica | Supervisionada | Classificação |
ID3 | Supervisionada | Classificação |
C4.5 | Supervisionada | Classificação |
One R | Supervisionada | Classificação |
PRISM | Supervisionada | Classificação |
K-NN | Supervisionada | Predição | --- |
Apriori | Não Supervisionada | Associação | --- |
K-Means | Não Supervisionada | Clustering | --- | Não Suporta |

**Tipos de Aprendizagem**

    - Supervisionada
    - Não Supervisionada

**Tipos de Problema**

    - Classificação - Predição
    - Regressão - Predição
    - Associação
    - Clustering

**Valores em Falta**

    - Suporta
    - Não suporta

**Normalização**

    - K-NN
    - K-Means



