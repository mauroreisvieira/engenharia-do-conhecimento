## Teste de Aferição de Conhecimentos 3
[👈 Voltar](../README.md)

**1. Descreva os conceitos de aprendizagem supervisionada e aprendizagem não supervisionada.**

    - Aprendizagem supervisionada
        - Utilizada para estimar uma dependência a partir dos dados de entrada/saída
        - Os problemas de predição (classificação e de regressão) são suportados por este tipo de aprendizagem
        - O termo “supervisionada” significa que os valores de saída das amostras de treino são conhecidos
        - A saída é a característica objetivo (label, class, etc.)

    - Aprendizagem não supervisionada
        - Utilizada para estimar uma dependência a partir dos dados de entrada
        - O termo “não supervisionada” significa que apenas os valores de entrada das amostras de treino são fornecidos ao sistema de aprendizagem
        - Os valores da saída são desconhecidos durante o processo de aprendizagem
        - Não existe uma característica objetivos

**2. Qual a diferença entre conjunto de dados de treino e conjunto de dados de teste?**

    - Conjunto de dados de treino pequeno
        - Modelo resultante pouco robusto
        - Fraca capacidade de generalização
    - Conjunto de dados de teste pequeno
        - Confiança no erro de generalização baixa
        - Vários métodos para estimar o erro

**3. Descreva os principais métodos que permitem separar os dados de treino e os dados de teste do conjunto inicial de dados.**

- Resubstituição
    - Método mais simples
    - Todas as amostras disponíveis são utilizadas para treino e para teste dos modelos
    - O conjunto de dados de treino e o conjunto de dados de teste são o mesmo
    - Método pouco utilizado em problemas reais de data mining
- Holdout
    - Metade dos dados, ou dois terços, ou então três quartos, são utilizados para treino e os restantes para teste
    - Os dados de treino e os dados de teste são independentes pelo que o erro de generalização é pessimista
    - A repetição do processo, com diferentes dados de treino e de teste, selecionados aleatoriamente, permite melhorar o modelo estimado (validação estatística)
- Leave-one-out
    - Um modelo é construído utilizando n-1 amostras para treino e avaliado com as restantes amostras
    - Este processo é repetido n vezes com diferentes conjuntos de treino de tamanho n-1
    - Esta abordagem exige muitos recursos computacionais pois é necessário construir e comparar muitos modelos
- Cross-validation
    - Compromisso entre o método holdout e o método leave-one-out
    - Dividem-se os dados em P subconjuntos do mesmo tamanho, 1≤P≤n
    - P-1 subconjuntos são utilizados para treino e os restantes subconjuntos são utilizados para teste
    - Método mais utilizado em problemas reais de data mining
- Bootstrap
    - Na sua forma mais simples, em vez de se analisar repetidamente subconjuntos dos dados, analisam-se subamostragens aleatórias com substituição da amostra completa
    - Permite gerar conjuntos de dados “falsos”, com o mesmo tamanho do conjunto de dados original
    - Este método é útil para conjuntos de dados com poucas amostras

**4. Considere um conjunto de dados constituído por 100 amostras (30 positivas e 70 negativas) em que o modelo classifica bem 10 das 30 amostras positivas e 65 das 70 negativas.**

 - Das 100 amostras 30 são positivas e 70 negativas.
 - 10 das 30 o modelo classifica bem.
 - 65 das 70 classifica negativa.

--- | P | N
--- | --- | ---
P | VP | FN
N | FP | VN

--- | P | N
--- | --- | ---
P | 10 | 20
N | 5 | 65

    a) Indique o número de Falsos Positivos : 5
    b) Indique o número de Falsos Negativos : 20
    c) Calcule a taxa de erro : 
        Taxa de Erro = FP + (FN / TotalAmostras)
        = 5 + 20 / 100 (Total de Amostras) = 0.25
    d) Calcule a taxa de acerto :
        Taxa de Acerto = 1 - TaxaErro
        1 - 0.25 (Taxa de Erro) = 0.75

**5. Faça a correspondência entre as tarefas de aprendizagem no contexto do data mining (classificação; regressão; clustering; sumarização; modelação de dependências; deteção de desvios e alterações) e as figuras que se apresentam a seguir.**

Fig 1 - 
Fig 2 - Classificação
Fig 3 - 
Fig 4 - Regressão
Fig 5 - 
Fig 6 - Clustering

![Pergunta 5 - Teste 3](./assets/teste3.png)
