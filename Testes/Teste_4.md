## Teste de Aferição de Conhecimentos 4
[👈 Voltar](../README.md)

**1. Para os seguintes algoritmos estudados no âmbito da unidade curricular, a saber, Naive Bayes, Regressão linear, Regressão logística, ID3, C4.5 (J48), One-R, PRISM e K-NN faça a sua caracterização para as seguintes propriedades:**

Algoritmo | Tipo de Aprendizagem | Tipo de Problema | Tipo de Atributos | Valores em Falta | Normalização dos Dados | Transparencia do Modelo |
--- | --- | --- | --- | --- | --- | --- |
Bayes Bayes | Supervisionada | Classificação |
Regressão Linear | Supervisionada | Regressão |
Regressão Logisitica | Supervisionada | Classificação |
ID3 | Supervisionada | Classificação |
C4.5 | Supervisionada | Classificação |
One R | Supervisionada | Classificação |
PRISM | Supervisionada | Classificação |
K-NN | Supervisionada | Predição | --- |
Apriori | Não Supervisionada | Associação | --- |
K-Means | Não Supervisionada | Clustering | --- | Não Suporta |


**2. Descreva a estrutura e os parâmetros dos modelos obtidos para cada um dos algoritmos referidos no exercício anterior (Modelo = Estrutura + Parâmetros).**


**3. Para o conjunto de dados apresentado na tabela seguinte utilize o algoritmo Naive Bayes para classificar a nova amostra.**

- Cálculo das probabilidades a priori
    - Classe 1
        - P(C=1) = 4/7 = 0.5714
    - Classe 2
        - P(C=2) = 3/7 = 0.4286

- Cálculo das probabilidades condicionadas
    - **Atributo A1**
        - P(A1=1/C=1) = 2/4 = 0.50
        - P(A1=1/C=2) = 1/3 = 0.33
    - **Atributo A2**
        - P(A2=2/C=1) = 1/4 = 0.25
        - P(A2=2/C=2) = 2/3 = 0.66
    - **Atributo A3**
        - P(A3=2/C=1) = 1/4 = 0.25
        - P(A3=2/C=2) = 2/3 = 0.66

- Assumindo a independência de atributos, as probabilidades condicionadas são:
    - P(X/C=1) = P(A1=1/C=1) * P(A2=2/C=1) * P(A3=2/C=1) = 0.50 * 0.25 * 0.25 = 0.03125
    - P(X/C=2) = P(A1=1/C=2) * P(A2=2/C=2) * P(A3=2/C=2) = 0.33 * 0.66 * 0.66 = 0.14375

- Multiplicando pelas probabilidades a priori
    - P(C1/X) ≈ P(X/C=1) * P(C=1) = 0.03125 * 0.5714 = 0.0179
    - P(C2/X) ≈ P(X/C=2) * P(C=2) = 0.14375 * 0.4286 = 0.0616

Amostra | Atributo 1 | Atributo 2 | Atributo 3 | Class C |
--- | --- | --- | --- | --- |
1 | 1 | 2 | 1 | 1 |
2 | 0 | 0 | 1 | 1 |
3 | 2 | 1 | 2 | 2 |
4 | 1 | 2 | 1 | 2 |
5 | 0 | 1 | 2 | 1 |
6 | 2 | 2 | 2 | 2 |
7 | 1 | 0 | 1 | 1 |
**8** | **1** | **2** | **2** | **?** |

**Conclusão:** Amostra pertence à _Classe 2_

**4. Indique em que circunstâncias se devem utilizar as técnicas de regressão linear e para que tipo de variáveis (características ou atributos) são apropriadas.**

- Apenas para atributos númericos.
- O objetivo da análise de regressão é determinar o melhor modelo que relacione a variável de saída (Y) com várias variáveis de entrada.


**7. Para o conjunto de dados apresentado na tabela seguinte e considerando 5-NN e a distância Euclidiana, como função de distância, apresente o resultado para a classificação do novo cliente (David).**

| Cliente | Idade | Rendimento | Cartões de crédito | Classe |
| --- | --- | --- | --- | --- |
| João | 35 | 35 | 3 | Não |
| Raquel | 22 | 50 | 2 | Sim |
| Ana | 63 | 200 | 1 | Não |
| António | 59 | 170 | 1 | Não |
| Maria | 25 | 40 | 4 | Sim |
| **David** | **37** | **50** | **2** | **?** |

| Cliente | Idade | Rendimento | Cartões de crédito | Classe |
| --- | --- | --- | --- | --- |
| João | 55/63 = 0.55 |  35/200 = 0.175 | 3/4 = 0.75 | Não |
| Raquel | el 22/63 = 0.34  | 50/200 = 0.25  | 2/4 = 0.5 | Sim |
| Ana | 63/63 = 1 | 200/200 = 1 | 1/4 = 0.25 | Não |
| António | 59/63 = 0.93 | 170/200 = 0.85 | 1/4 = 0.25 | Não |
| Maria | 25/63 = 0.39 | 40/200 = 0.2 | 4/4 = 1 | Sim |
| **David** | **37/63 = 0.58** | **50/200 = 0.25** | **2/4 = 0.5** | **Sim** |



