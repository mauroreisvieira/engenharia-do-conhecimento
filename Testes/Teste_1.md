## Teste de Aferição de Conhecimentos 1
[👈 Voltar](../README.md)

**1. Defina, sucintamente, o conceito de data mining.**

É o processo de extração de informação e conhecimento relevante a partir de grandes quantidades de dados.

**2. Preencha a figura com as designações das etapas que compõem a metodologia CRISP-DM.**

CRISP-DM - CRoss-Industry Standard Process for Data Mining

- Compreensão do negócio
    - Definir os objetivos de negócio
    - Avaliar o cenário
    - Definir os objetivos de data mining
    - Elaborar o plano de projeto

- Compreensão dos dados
    - Recolha dos dados iniciais
    - Descrição e Exploração dos dados
    - Verificação da qualidade dos dados
- Preparação dos dados
    - Seleção dos dados e Limpeza dos dados
    - Derivar nos dados
    - Criaçao do data set
- Modelização
    - Escolher tecnicas de modelização
    - Separação de dados em conjuntos de treino
    - Avaliação do modelo
- Avaliação
    - Avaliação dos resultados
    - Revisão do processo
    - Recomendação da implementação do projeto
- Colocação em produção
    - Planeamento da colocação em produção
    - Plano de monitorização
    - Relatório
    - Apresentação

**4. Faça a correspondência entre as etapas da metodologia CRISP-DM, coluna da esquerda, e as etapas da metodologia SEMMA do SAS Institute, coluna da direita.**

| --- | Etapas da Metodologia |  CRISP-DM Etapas da Metodologia SEMMA | --- |
| --- | --- | --- | --- |
| A | Compreensão do negócio | Amostragem dos dados | 1 |
B | Compreensão dos dados | Exploração dos dados | 2 |
C | Preparação dos dados | Modificação dos dados | 3 |
D | Modelização Modelação | dos dados | 4 |
E | Avaliação | Avaliação dos modelos obtidos | 5 |
F | Colocação em produção | --- |

**5. Para cada uma das reuniões levadas a cabo numa organização, descritas na coluna da direita, indique a etapa da metodologia CRISP-DM, coluna da esquerda, à qual deverá estar associada.**

|  | Etapas da Metodologia CRISP-DM | Descrição da Reunião |  |
| --- | --- | --- | --- |
| A | Compreensão do negócio | Os gestores reúnem para discutir o impacto do projeto no processo de negócio da empresa. | 1 |
| B | Compreensão dos dados | O consultor de data mining reúne com o diretor de marketing para definir objetivos. | 2 |
| C | Preparação dos dados | O gestor do projeto de data mining reúne com a equipa para discutir como é que os dados vão ser integrados. | 3 |
| D | Modelização | O gestor do projeto de data mining reúne com o responsável dos sistemas de informação para discutir a colocação do modelo no servidor aplicacional. | 4 |
| E | Avaliação | O gestor do projeto de data mining reúne com o responsável da produção para analisar o relatório de qualidade dos dados. | 5 |
| F | Colocação em produção | Os analistas reúnem para decidir se devem utilizar redes neuronais ou árvores de decisão na construção dos modelos. | 6 |

**6. Faça a correspondência entre as tarefas genéricas das várias etapas da metodologia CRISP-DM, coluna da esquerda, e as descrições apresentadas na coluna da direita.**

|  | Tarefa Genérica | Descrição do Tarefa Genérica |  |
| --- | --- | --- | --- |
| A | Integração de dados | Avaliação do modelo e afinação dos parâmetros | 1 |
| B | Planeamento de testes | Determinar os objetivos e os critérios de sucesso do projeto | 2 |
| C | Exploração dos dados | Avaliar os resultados tendo em conta os critérios de sucesso definidos | 3 |
| D | Recolha dos dados | Separação dos dados em conjuntos de treino e teste | 4 |
| E | Avaliar o cenário | Criar novas características e gerar amostras | 5 |
| F | Determinar ações futuras | Inventário de recursos, requisitos, restrições, riscos, custos e benefícios | 6 |
| G | Derivar novos dados | Recomendação de implementação ou redefinição do problema | 7 |
| H | Avaliação de resultados | Documentar como foram obtidos os dados | 8 |
| I | Avaliação do modelo | Junção de dados adicionais | 9 |
| J | Objetivos de data mining | Documentar as tarefas de exploração dos dados iniciais | 10 |








