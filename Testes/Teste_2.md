## Teste de Aferição de Conhecimentos 2
[👈 Voltar](../README.md)

**Pergunta 1:  Por que razão a análise exploratória de dados é fundamental no processo de data mining? Fará sentido prosseguir diretamente para a etapa de modelização sem previamente levar a cabo uma análise exploratória dos dados?**

Não, porque poderá originar uma falta de visão sobre a informação existente nos dados, levando a dificuldades técnicas em interpretar e ligar os dados.

**Para cada uma das seguintes técnicas/métodos descritivos indique a que tipos de características (atributos) podem ser aplicados: numéricas, categóricas ou ambas.**

| Técnica/Método Descritivo | Numéricas | Categóricas | Ambas |
| - | - | - | - |
| Gráfico de barras | | | X |
| Histograma | | | X |
| Medidas estatísticas | X | | |
| Análise de correlações | X | | |
| Gráficos de matrizes de pontos |X | | |
| Grafos | X | | |
| Binarização (binning) | | X | |

**Classifique as seguintes características (atributos) com base numa ou mais das seguintes opções: binária, discreta, contínua, qualitativa ou quantitativa.**

| Característica (Atributo) | Binária | Discreta | Contínua | Qualitativa | Quantitativa |
| - | - | - | - | - | - |
| Idade | | | | | X |
| Hora | | | | | |
| Luminosidade medida por um instrumento | X | | | | |
| Luminosidade medida por uma pessoa | | X | | | |
| Ângulo | | | X | | |
| Medalha (e.g., Jogos Olímpicos) | | | | X | |
| Número de estudantes num turno | | | | | X |
| Aproveitamento numa UC | X | | | | |
| ISBN de um livro | | | | | |
| Qualificações académicas | | | | | |
| Distância | | | | | |
| Senha de espera (e.g., Loja do Cidadão) | | | | | |

**Indique as razões pelas quais não é possível automatizar completamente a etapa de preparação de dados, do processo de data mining, e ao invés a presença e acompanhamento humano são indispensáveis**

Os dados originais precisam de ser transformados para poderem ser utilizados pelos algoritmos de data mining. As transformações não são ótimas e são feitas muitas vezes manualmente, sendo fundamental a experiência humanos.

**6. Construa, numa ferramenta computacional á sua escolha, um pequeno conjunto de dados com as classificações finais obtidas por dez estudantes na unidade curricular de Engenharia do Conhecimento, em que uma das classificações é obrigatoriamente um outlier.**
//TODO:

    - a) Determine o valor da média, da mediana e do desvio padrão, com e sem a presença da amostra considerada outlier.
    //TODO:

    - b) Qual das medidas (média, mediana ou desvio padrão) é mais afetada pela presença da amostra considerada outlier. Porquê?
    //TODO:

    - c) Verifique que a amostra considerada um outlier o é de facto, utilizando para o efeito a abordagem estatística e a abordagem baseada numa distância.
    //TODO:

    - d) Remova a amostra considerada outlier do conjunto de dados e efetue a normalização dos dados utilizando os três métodos estudados nas aulas teóricas, a saber: escala decimal, min-max e desvio padrão.
    //TODO:

**7. Após a etapa de preparação de dados, a presença de características correlacionadas no data set irá dar demasiada ênfase a determinadas componentes dos dados e produzir resultados em princípio pouco fiáveis.**

    - a) Como podemos determinar se existe correlação entre as características de um data set?
        Determinando a matriz de correlação das caracteristicas do DataSet, e determinar se existem valores próximos de 1 ou -1. Valores próximos de 1 ou -1 indicam correlações fortes.

    - b) Que passos podem ser levados a cabo para corrigir esta situação?
        Remover uma das caracteristicas.

**8. A etapa de preparação de dados compreende um conjunto bastante diversificado de tarefas, entre as quais a redução de dados.**

    - a) Indique os três tipos de redução de dados
        - Redução de características
        - Redução de casos ou amostras
        - Redução de valores

**b) Quais os três parâmetros que devem ser tidos em conta na redução de dados? É possível melhorar os três em simultâneo? Justifique a sua resposta.**
//TODO:
