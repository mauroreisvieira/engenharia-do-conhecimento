# Engenharia do Conhecimento

### Resumo Aulas Teóricas
- [Introducao data mining](./Aula_01_Introducao_Data_Mining.md)
- [Exploração de dados](./Aula_02_Exploracao_de_Dados.md)
- [Preparação dos dados](./Aula_03_Preparacao_de_Dados.md)
- [Redução dos dados](./Aula_04_Reducao_Dados.md)
- [Aprendizagem a partir dos dados](./Aula_05_Aprendizagem_partir_dos_Dados.md)
- [Metodos Estaticos](./Aula_06_Metodos_Estaticos.md)
- [Arvores e Regras de Decisao](./Aula_07_Arvores_Regras_Decisao.md)
- [Memory-Based Learning](./Aula_08_Memory-Based_Learning.md)
- [Avaliação e Seleção de Modelos](./Aula_09_Avaliacao_Selecao_Modelos.md)
- [Regras de Associacao](./Aula_10_Regras_Associacao.md)
- [Clustering](./Aula_11_Clustering.md)


### Testes de Aferição de Conhecimentos
- [Teste 1](./Testes/Teste_1.md)
- [Teste 2](./Testes/Teste_2.md)
- [Teste 3](./Testes/Teste_3.md)
- [Teste 4](./Testes/Teste_4.md)
- [Teste 5](./Testes/Teste_5.md)
- [Teste 6](./Testes/Teste_6.md)


### Outras Revisões
- [Revisões](./Revisoes.md)
